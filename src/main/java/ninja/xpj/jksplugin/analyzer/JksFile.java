package ninja.xpj.jksplugin.analyzer;

import java.io.IOException;
import java.io.InputStream;

public interface JksFile {

    String getJksFileName();
    InputStream getJksInputStream() throws IOException;

}
