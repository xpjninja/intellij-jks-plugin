package ninja.xpj.jksplugin.analyzer;

import com.intellij.openapi.vfs.VirtualFile;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.io.InputStream;

@RequiredArgsConstructor
public class JksVirtualFile implements JksFile {

    private final VirtualFile virtualFile;

    @Override
    public String getJksFileName() {
        return virtualFile.getName();
    }

    @Override
    public InputStream getJksInputStream() throws IOException {
        return virtualFile.getInputStream();
    }
}
