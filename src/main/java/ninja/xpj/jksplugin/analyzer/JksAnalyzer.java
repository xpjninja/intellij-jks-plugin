package ninja.xpj.jksplugin.analyzer;

import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import ninja.xpj.jksplugin.formatter.CertificateFormatter;

import javax.swing.text.EditorKit;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
public class JksAnalyzer {

    private static final List<String> KEYSTORE_TYPES = Lists.newArrayList("jks", "pkcs12", "jceks", "dks", "pkcs11");

    private final JksFile jksFile;
    private final CertificateFormatter certificateFormatter;
    private KeyStore javaKeyStore;

    public void loadKeyStore(char[] password, String jksType) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
            javaKeyStore = KeyStore.getInstance(jksType);
            javaKeyStore.load(jksFile.getJksInputStream(), password);
    }

    public List<String> getAliases() throws KeyStoreException {
        return Collections.list(javaKeyStore.aliases());
    }

    public String getJksFileName() {
        return jksFile.getJksFileName();
    }

    public String getCertificateDetails(String alias) throws KeyStoreException {
        Certificate certificate = javaKeyStore.getCertificate(alias);
        return certificateFormatter.format(certificate);
    }

    public static List<String> getKeystoreTypes() {
        return KEYSTORE_TYPES;
    }

    public CertificateFormatter getCertificateFormatter() {
        return certificateFormatter;
    }
}
