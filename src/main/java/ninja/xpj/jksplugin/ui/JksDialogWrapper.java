package ninja.xpj.jksplugin.ui;

import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.ui.CollectionComboBoxModel;
import ninja.xpj.jksplugin.analyzer.JksAnalyzer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.StringReader;
import java.security.KeyStoreException;
import java.util.Vector;

public class JksDialogWrapper extends DialogWrapper {

    private final JksAnalyzer jksAnalyzer;
    private JksMainWindow jksMainWindow;

    public JksDialogWrapper(JksAnalyzer jksAnalyzer) {
        super(true);
        this.jksAnalyzer = jksAnalyzer;
        this.jksMainWindow = new JksMainWindow();

        init();
        localInit();
    }

    private void localInit() {
        setModal(false);
        setTitle("JKS Analyzer: " + jksAnalyzer.getJksFileName());
        jksMainWindow.getJksTypeComboBox().setModel(new CollectionComboBoxModel<>(JksAnalyzer.getKeystoreTypes()));
        jksMainWindow.getAnalyzeButton().addActionListener(analyzeButtonAction());
        jksMainWindow.getJksAliasesList().addMouseListener(aliasesListMouseClick());
        jksMainWindow.getJksAliasesList().addKeyListener(aliasesListKeyListener());
    }

    private KeyListener aliasesListKeyListener() {
        return new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    showCertificateDetail((String)jksMainWindow.getJksAliasesList().getSelectedValue());
                }
            }
        };
    }

    private MouseAdapter aliasesListMouseClick() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    showCertificateDetail((String)jksMainWindow.getJksAliasesList().getSelectedValue());
                }
            }
        };
    }

    private void showCertificateDetail(String alias) {
        try {
            JTextPane jksDetailsField = jksMainWindow.getJksDetailsField();
            EditorKit editorKit = jksDetailsField.getEditorKitForContentType(jksAnalyzer.getCertificateFormatter().getContentType());
            Document document = editorKit.createDefaultDocument();

            editorKit.read(new StringReader(jksAnalyzer.getCertificateDetails(alias)), document, 0);

            jksDetailsField.setDocument(document);

        } catch (KeyStoreException | IOException | BadLocationException e) {
            Messages.showErrorDialog(e.getMessage(), "JSK Analysis Error");
        }
    }


    private ActionListener analyzeButtonAction() {
        return a -> {
            try {
                char[] jksPassword = jksMainWindow.getJksPasswordField().getPassword();
                String jksType = (String)jksMainWindow.getJksTypeComboBox().getSelectedItem();
                jksAnalyzer.loadKeyStore(jksPassword, jksType);
                jksMainWindow.getJksAliasesList().setListData(new Vector<>(jksAnalyzer.getAliases()));
            } catch (Exception e) {
                Messages.showErrorDialog(e.getMessage(), "JSK Analysis Error");
            }
        };
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return jksMainWindow.getPanel();
    }

    @Override
    public String getDimensionServiceKey() {
        return "#" + this.getClass().getCanonicalName();
    }

    @NotNull
    @Override
    protected Action[] createActions() {
        return new Action[]{getOKAction()};
    }
}
