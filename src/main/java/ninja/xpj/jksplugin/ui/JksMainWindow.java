package ninja.xpj.jksplugin.ui;

import javax.swing.*;

public class JksMainWindow extends JDialog {
    private JPanel contentPane;
    private JPasswordField jksPasswordField;
    private JButton analyzeButton;
    private JTextPane jksDetailsField;
    private JList jksAliasesList;
    private JComboBox<String> jksTypeComboBox;

    public JksMainWindow() {
        setContentPane(contentPane);
        setModal(true);
    }

    public JPanel getPanel() {
        return contentPane;
    }

    public JButton getAnalyzeButton() {
        return analyzeButton;
    }

    public JList getJksAliasesList() {
        return jksAliasesList;
    }

    public JTextPane getJksDetailsField() {
        return jksDetailsField;
    }

    public JPasswordField getJksPasswordField() {
        return jksPasswordField;
    }

    public JComboBox getJksTypeComboBox() {
        return jksTypeComboBox;
    }
}
