package ninja.xpj.jksplugin.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.vfs.VirtualFile;
import ninja.xpj.jksplugin.analyzer.JksAnalyzer;
import ninja.xpj.jksplugin.analyzer.JksVirtualFile;
import ninja.xpj.jksplugin.formatter.SimpleCertificateFormatter;
import ninja.xpj.jksplugin.ui.JksDialogWrapper;
import org.jetbrains.annotations.NotNull;

public class JksContextMenuAction extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        VirtualFile file = e.getData(CommonDataKeys.VIRTUAL_FILE);
        if (isJksFile(file)) {
            JksAnalyzer jksAnalyzer = new JksAnalyzer(new JksVirtualFile(file), SimpleCertificateFormatter.getInstance());
            new JksDialogWrapper(jksAnalyzer).show();
        }
    }

    @Override
    public void update(AnActionEvent e) {
        VirtualFile file = e.getData(CommonDataKeys.VIRTUAL_FILE);
        e.getPresentation().setEnabledAndVisible(isJksFile(file));
    }

    private boolean isJksFile(VirtualFile file) {
        return file != null && file.getName().endsWith(".jks");
    }
}
