package ninja.xpj.jksplugin.formatter;

import java.security.cert.Certificate;

public interface CertificateFormatter {
    Class<?> supportedClass();
    String format(Certificate certificate);
    String getContentType();
}
