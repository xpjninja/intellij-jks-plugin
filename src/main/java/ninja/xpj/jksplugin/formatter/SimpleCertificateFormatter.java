package ninja.xpj.jksplugin.formatter;

import java.security.cert.Certificate;

public class SimpleCertificateFormatter implements CertificateFormatter {

    private static final CertificateFormatter INSTANCE = new SimpleCertificateFormatter();

    @Override
    public Class<?> supportedClass() {
        return null;
    }

    @Override
    public String format(Certificate certificate) {
        return certificate.toString();
    }

    @Override
    public String getContentType() {
        return "text/plain";
    }

    public static CertificateFormatter getInstance() {
        return INSTANCE;
    }
}
