package ninja.xpj.jksplugin.formatter;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public class HTMLCertificateFormatter implements CertificateFormatter {

    @Override
    public Class<?> supportedClass() {
        return X509Certificate.class;
    }

    @Override
    public String format(Certificate certificate) {
        if (supportedClass().isAssignableFrom(certificate.getClass())) {
            X509Certificate x509certificate = (X509Certificate)certificate;

            return "";
        }
        return "";
    }

    @Override
    public String getContentType() {
        return "text/html";
    }
}
