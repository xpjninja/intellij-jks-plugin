package icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public interface Icons {

    Icon JKS_ANALYZER_ICON = IconLoader.getIcon("/icons/icon.png");

}
