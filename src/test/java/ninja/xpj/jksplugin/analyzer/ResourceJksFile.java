package ninja.xpj.jksplugin.analyzer;

import java.io.InputStream;

public class ResourceJksFile implements JksFile {

    private final String filename;

    public ResourceJksFile(String filename) {
        this.filename = filename;
    }

    @Override
    public String getJksFileName() {
        return filename;
    }

    @Override
    public InputStream getJksInputStream() {
        return this.getClass().getResourceAsStream(filename);
    }
}
