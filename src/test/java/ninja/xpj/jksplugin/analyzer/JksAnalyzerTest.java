package ninja.xpj.jksplugin.analyzer;

import com.google.common.collect.Lists;
import ninja.xpj.jksplugin.formatter.SimpleCertificateFormatter;
import org.junit.Test;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JksAnalyzerTest {

    @Test
    public void createsJksAnalyzer() {
        JksAnalyzer jksAnalyzer = new JksAnalyzer(new ResourceJksFile("/clientkeystore.jks"), null);

        assertEquals("/clientkeystore.jks",jksAnalyzer.getJksFileName());
    }

    @Test
    public void loadsKeyStore() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        JksAnalyzer jksAnalyzer = new JksAnalyzer(new ResourceJksFile("/clientkeystore.jks"), null);
        jksAnalyzer.loadKeyStore("123456".toCharArray(), "jks");

        assertEquals(1, jksAnalyzer.getAliases().size());
        assertEquals("client", jksAnalyzer.getAliases().get(0));
    }

    @Test(expected = IOException.class)
    public void throwsErrorOnIncorrectPassword() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        JksAnalyzer jksAnalyzer = new JksAnalyzer(new ResourceJksFile("/clientkeystore.jks"), null);
        jksAnalyzer.loadKeyStore("654321".toCharArray(), "jks");
    }

    @Test(expected = IOException.class)
    public void throwsErrorOnIncorrectKeyStoreFormat() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        JksAnalyzer jksAnalyzer = new JksAnalyzer(new ResourceJksFile("/clientkeystore.jks"), null);
        jksAnalyzer.loadKeyStore("654321".toCharArray(), "jceks");
    }

    @Test
    public void checkCertificate() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        JksAnalyzer jksAnalyzer = new JksAnalyzer(new ResourceJksFile("/clientkeystore.jks"), SimpleCertificateFormatter.getInstance());
        jksAnalyzer.loadKeyStore("123456".toCharArray(), "jks");
        String client = jksAnalyzer.getCertificateDetails("client");

        assertNotNull(client);
    }

    @Test
    public void keyStoreTypes() {
        assertEquals(Lists.newArrayList("jks", "pkcs12", "jceks", "dks", "pkcs11"), JksAnalyzer.getKeystoreTypes());
    }

}