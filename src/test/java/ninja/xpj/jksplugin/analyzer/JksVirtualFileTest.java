package ninja.xpj.jksplugin.analyzer;

import com.intellij.mock.MockVirtualFile;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class JksVirtualFileTest {

    @Test
    public void createJksVirtualFile() {
        MockVirtualFile mockVirtualFile = MockVirtualFile.file("keystore.jks");
        JksVirtualFile jksVirtualFile = new JksVirtualFile(mockVirtualFile);

        assertEquals("keystore.jks", jksVirtualFile.getJksFileName());
    }

}